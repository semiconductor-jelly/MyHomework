/*
* 1,不解释
* 2,字符串的切割,分割文件路径,文件名,拓展名
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project1
{
    class Class0
    {
        static void Main(string[] args)
        {
            string id = "0001";
            string name = "X洗发水";
            decimal inprice = 10M;
            float profit = 0.5f;
            decimal outprice = 15M;

            Console.WriteLine("{0,30}", "商品表");
            Console.WriteLine();
            Console.Write("货号");
            Console.Write("{0,10}", "商品名称");
            Console.Write("{0,10}", "进货价（元）");
            Console.Write("{0,8}", "利润率");
            Console.WriteLine("{0,10}", "售价（元）");
            string s = string.Format("{0,-10}{1,-10}{2,-15: ￥ 0.0}｛3,-12: 0.00%｝{4,-10:" + "￥0.0}", id, name, inprice, profit, outprice);
            Console.WriteLine(s);
            Console.WriteLine();

            Cut();

        }
        public static int Cut()
        {
            Console.WriteLine("输入需要切割的字符串");
            String path = null, fileName = null, extension = null, x = Console.ReadLine();
            path = x.Substring(0,/*起始位置: 0*/ x.LastIndexOf("\\") + 1/*截取长度:最后一个"\\"的位置索引 + 1*/);
            fileName = x.Substring(x.LastIndexOf("\\") + 1/*起始位置:最后一个"\\"的位置索引 + 1*/ , x.LastIndexOf(".") - x.LastIndexOf("\\")-1/*截取长度:最后一个"."的位置索引与最后一个"\\"位置索引的差 -1 */);
            extension = x.Substring(x.LastIndexOf(".") + 1, x.Length - x.LastIndexOf(".") - 1);
            Console.WriteLine("文件路径:" + path);
            Console.WriteLine("文件名:" + fileName);
            Console.WriteLine("拓展名:" + extension);

            return 0;
        }
    }
}