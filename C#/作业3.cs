/*
* 回文判断(回文就是正反读一样的字符串)
*/

using System;

namespace ConsoleApp1
{
    class Class2
    {
        public static void Main(string[] args)
        {
            char[] y = Console.ReadLine().ToCharArray();
            int z = y.Length / 2;
            for (int i = 0; i < z; i++)
            {
                if (!(y[i] == y[y.Length - 1 - i]))
                {
                    Console.WriteLine("False");
                    Main(null);
                    return;
                }
            }
            Console.WriteLine("True");
            Main(null);
        }
    }
}
