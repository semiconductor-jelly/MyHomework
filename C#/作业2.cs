/*
* 测试string与stringBuilder拼接字符的效率
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Class1
    {
        public static void Main(string[] args)
        {
            string x = "0";
            StringBuilder y = new StringBuilder("0");
            long t;
            t = DateTime.Now.Ticks;
            for(int i = 0;i<10000;i++)
            {
                x += i;
            }
            Console.WriteLine("string耗时{0:D}\n",DateTime.Now.Ticks - t);
            t = DateTime.Now.Ticks;
            for(int i = 0;i<10000;i++)
            {
                y.Append(i);
            }
            Console.WriteLine("stringBuilder耗时{0:D}\n",DateTime.Now.Ticks - t);
            Console.ReadLine();
        }
    }
}
